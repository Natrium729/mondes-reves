#let title = "Mondes rêvés"

#set document(
	title: title,
	author: "Nathanaël Marion",
)

#let body-size = 10pt
#let leading = .6em
#let h1-size = body-size * 1.2

#let hr() = align(center, block(
	height: 0pt,
	width: 30%,
	above: body-size * 1.5,
	below: body-size * 1.5,
	"***"
))

#set page(
	width: 11cm,
	height: 18cm,
)

#set text(
	lang: "fr",
	font: "Alegreya",
	size: body-size,
)

#set par(
	first-line-indent: 1.25em,
	leading: leading,
	linebreaks: "optimized",
	// justify: true,
)

#show par: set block(spacing: leading)

#show heading.where(level: 1): it => {
	pagebreak(weak: true)
	block(below: 24pt)[
		#set text(size: h1-size, weight: "bold")
		#it.body
	]
}

#show heading.where(level: 2): it => [
	#set text(size: body-size, weight: "regular", style: "italic")
	#block(above: body-size * 1.5, below: 0pt)
	#smallcaps(it.body).
	// #parbreak()
	// #block(above: leading, below: 0pt)
	#h(.25em)
]

#show ref: it => {
	let el = it.element

	if el != none and el.func() == heading and el.level == 2 {
		// set text(tracking: 1em * 0.05) // TODO
		link(el.label, smallcaps(el.body))
	} else {
		it
	}
}

// Page de titre.
#page()[
	#show: rest => {
		align(center, rest)
	}

	#v(20%)

	#text(30pt)[#smallcaps[#title]]

	#v(body-size)

	#text(12pt)[Nathanaël Marion]

]

#page()[
	#show: rest => align(center + bottom, rest)
	#set text(size: .75em)
	#align(bottom)[
		version 1 \
	]
]

#set page(
	numbering: "1",
	header: locate(loc => {
		// Get all entries after the heading
		// (i.e. from the current page to the end).
		let elems = query(
			heading.where(level: 2).after(loc),
			loc
		)
		// If there are entries,
		// and the first one is on the same page as the header,
		// show it on the left or right of the header.
		if (
			elems.len() != 0
			and elems.first().location().page() == loc.page()
		) {
			let pos = right
			if calc.even(loc.page()) {
				pos = left
			}
			align(pos, elems.first().body)
		}
	}),
)

= Avant-propos #footnote[Bien entendu, ce "petit" avant-propos de plus de 100 mots ne compte pas pour la limite des 500 mots ! Et bien entendu, il en va de même pour ces notes de bas de pages. #footnote[Note pour la prochaine fois : tricher et écrire une fiction interactive uniquement dans des notes de bas de page ne comptant pas dans la limite de mots ? #footnote[Je plaisante.]]]

// 127 mots

Ce livre-jeu a été écrit pour la Partim~500, an 2023 #footnote[https://itch.io/jam/fr-partim-500-an-2023], dont le thème était "dévoiement".

Je suis avant tout un auteur de fictions interactives (jeux textuels sur ordinateur) ; je n'ai à peu près aucune expérience en livre dont vous êtes le héros — hormis la poignée que j'ai lu quand j'étais plus jeune —, et je n'ai aucun contact avec ce qui se fait aujourd'hui en la matière. Je ne garantis donc rien quant à ce que vous êtes en train de lire en ce moment.

#hr()

Dans ce livre, les passages ne sont pas numérotés. Ils sont à la place identifiés par des mots #footnote[Mots que je n'ai pas non plus comptés.], rangés dans l'ordre alphabétique comme dans un dictionnaire.

Pour commencer, il suffit de vous rendre à la première entrée : @aoda.

#pagebreak()
#pagebreak()

= Mondes rêvés

// TODO: Compter mots intitulant chaque section?

== Aoda <aoda> // 16 mots.
Parfois, quand Aoda fait les comptes de l'hôtel, elle s'endort et @reve d'autres mondes.

== bas <bas> // 35 mots
La cascade de sable, du centre du ciel, apporte ses chansons au réceptacle inférieur. Pourtant, il suffit d'un hoquet, d'un @sursaut pour que la source se tarisse, pour que le temps s'arrête.

== chantent <chantent> // 38 mots
"Dans les profondeurs, la troisième chose que l'on voit contient la clef, si la haine se met à béer."

Aoda déconcertée perd pied avec le songe. Il n'y a plus que le @reveil qui l'attend.

== chemins <chemins> // 22 mots
Ce monde recouvert d'@ocean a déjà beaucoup subi. Celui-là, en forme de @sablier, a à peine eu le temps d'exister.

== ciel <ciel> // 27 mots
Le domaine céleste accueille Aoda, qui danse parmi les rayons et les nuages. Insouciante, elle remarque à peine le @sursaut qui agite la trame même du monde.

== haut <haut> // 42 mots
Les dunes @chantent les histoires d'éons passés, avant que la fêlure zébrant la paroi du sablier ne s'élargisse et que les grains de sable s'échappent tous. Le temps s'arrête sans le moindre @sursaut. Les dunes se sont tues.

== océan <ocean> // 21 mots
À perte de vue, la mer . Au-dessus, le @ciel limpide. Et dans les profondeurs --- Aoda le sent ---, la @terre qui gronde.

== orbe // 47 mots
Un souvenir, insaisissable : la nature de l'artefact se révèle à Aoda. Cette sphère comme le soleil éclatant n'est pas au démon. Une pensée, et l'objet roule vers le héros, qui n'était qu'inconscient. Il se lève, n'ayant en tête que la @victoire.

== rester <rester> // 31 mots
Alors Aoda reste le temps d'un songe. Elle ne voyage pas réellement, au contraire de ses clients, mais c'est mieux ainsi. Au @reveil, un sourire flotte sur ses lèvres.

== rêve <reve> // 26 mots
Certains mondes sont lumineux et donnent envie d'y @rester. D'autres sont… loin d'être parfaits et mériteraient de se diriger vers de meilleurs @chemins.

== réveil <reveil> // 21 mots
Bientôt, le souvenir de ce monde onirique s'estompera. Qu'importe ! il suffira d'un autre @reve pour voyager de nouveau…

== sablier <sablier> // 23 mots
Le pays d'en @haut se vide ; le pays d'en @bas se remplit. Les siècles s'écoulent en un battement de cils.

== sursaut <sursaut> // 32 mots
Aoda ouvre les yeux, le cœur battant. Le songe lui a échappé sans qu'elle puisse y faire quoi que ce soit. Mais peut-être, dans un autre @reve, dans un autre monde…

== terre <terre> // 37 mots
Le magma orne la pierre de couleurs sinistres. Un héros est mort dans des mâchoires infernales. Son artefact a été perverti par un démon, qui se sert de sa puissance. Les roches ont un @sursaut de colère.

== victoire <victoire> // 45 mots
Aoda ouvre doucement les yeux. Si beaucoup de mondes sont sur une mauvaise voie, elle sait que, grâce à elle, au moins l'un d'eux a une chance de retrouver le bon chemin.

Reposée, elle reprend son travail là où elle l'avait laissé.


#pagebreak()
#page(numbering: none)[
	#show: rest => align(center + bottom, rest)
	#set text(size: .75em)

	#let today = datetime.today()
	// Because Typst doesn't support localised datetimes at the time of writing this comment.
	#let months = ("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre")

	// The version won't be correct if someone compiles with another version, though.
	Compilé avec Typst version 0.5.0 le #today.day() #months.at(today.month() - 1) #today.year()
]
