# *Mondes rêvés*

> Parfois, quand Aoda fait les comptes de l'hôtel, elle s'endort et rêve d'autres mondes.

*Mondes rêvés* un tout petit livre-jeu de moins de 500 mots, écrit à l'occasion de [la Partim 500, an 2023](https://itch.io/jam/fr-partim-500-an-2023).

Il est disponible [sur itch.io](https://natrium729.itch.io/mondes-reves).

## Pour compiler la source

Le livre est écrit avec [Typst](https://typst.app/). On peut le compiler avec :

```
typst compile main.typ
```

(Mais il est possible que ça ne fonctionne pas avec une version plus récente que celle utilisée lors de l'écriture.)
